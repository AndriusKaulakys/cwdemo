var accordion = $('#accordion');

accordion.on('show.bs.collapse', function (e) {
    var panel = $(e.target).parent();
    var panelIcon = $('.accordion-icon', panel);
    var panelLoader = $('.accordion-loader', panel);
    var panelData = $('.accordion-data', panel);

    panelIcon.removeClass('fa-caret-right').addClass('fa-caret-down');
    panelLoader.show();
    panelData.hide();

    // simulate slow connection
    setTimeout(function () {
        $.ajax({
            type: 'GET',
            url: 'api/ajax.json',
            success: function (response) {
                panelData.html(response.data);
                panelLoader.hide();
                panelData.fadeIn('slow');
            },
            error: function () {
                panelData.text('error!');
                panelLoader.hide();
                panelData.fadeIn('slow');
            }
        });
    }, 1000);
});

accordion.on('hide.bs.collapse', function (e) {
    var panel = $(e.target).parent();
    var panelIcon = $('.accordion-icon', panel);
    var panelData = $('.accordion-data', panel);

    panelIcon.removeClass('fa-caret-down').addClass('fa-caret-right');
    panelData.fadeOut('slow').empty();
});

$("#accordion-form").validate({
    submitHandler: function (form) {
        form.submit();
    },
    highlight: function (element) {
        var formGroup = $(element).parent();
        var formIcon = $('.glyphicon', formGroup);
        formIcon.addClass('glyphicon-remove').removeClass('glyphicon-ok');
        formGroup.addClass('has-error').removeClass('has-success');
    },
    success: function (label) {
        var formGroup = $(label).parent();
        var formIcon = $('.glyphicon', formGroup);
        formIcon.addClass('glyphicon-ok').removeClass('glyphicon-remove');
        formGroup.addClass('has-success').removeClass('has-error');
    },
    errorClass: 'error help-block',
    rules: {
        inputName: 'required',
        inputSurname: 'required',
        inputEmail: {
            required: true,
            email: true
        },
        inputPhone: {
            required: true,
            digits: true
        },
        inputMessage: 'required'
    },
    messages: {
        inputName: 'Name is required',
        inputSurname: 'Surname is required',
        inputEmail: {
            required: 'Email is required',
            email: 'Your email address must be in the format of name@domain.com'
        },
        inputPhone: {
            required: 'Phone is required',
            digits: 'Phone must be only digits'
        },
        inputMessage: 'Message is required'
    },
});