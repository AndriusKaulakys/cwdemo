####Components:
* bootstrap 3
* font-awesome
* jquery
* jquery-validation

####Requirements for development:
* npm
* gulp

####To start development:
* `git clone ... cwdemo && cd cwdemo`
* `npm install`
* `gulp serve`
* `open http://localhost:9001`